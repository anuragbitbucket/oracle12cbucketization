package com.anurag.beta;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

//Beta in out program the ratio of time taken for a query to run on encrypted data 
//and the time it takes to run on plain text data
public class EstimateBeta {

	private static final int NUMBER_OF_SIMULATION = 50;
	private int simulationNumber = -1;
	private BigDecimal[] keys = new BigDecimal[NUMBER_OF_SIMULATION];
	private long[] timeEncryptedNonDet = new long[NUMBER_OF_SIMULATION];
	private long[] timeEncryptedDet = new long[NUMBER_OF_SIMULATION];
	private long[] timePlainText = new long[NUMBER_OF_SIMULATION];

	private static String plainTextTable = "CUSTOMER_BASE";
	private static String encryptedNonDet = "CUSTOMER_ALL_ENCRYPTED";
	private static String encryptedDet = "CUSTOMER_DETERMINISTIC";

	public static void main(String[] args) throws SQLException, FileNotFoundException {
		EstimateBeta eb= new EstimateBeta();
		eb.run();
		eb.print();
		
	}
	
	public void print() throws FileNotFoundException{
		PrintStream ps =new PrintStream(new FileOutputStream("BetaValues.txt"));
		
		for(int i = 0; i <NUMBER_OF_SIMULATION ; i++){
			ps.println(
					keys[i] + "\t"+ 
					timePlainText[i] + "\t" + 
					timeEncryptedDet[i] + "\t" + 
					timeEncryptedNonDet[i]);
			
		}
		ps.println("\n\n\n");

		
	}
	
	public void run() throws SQLException {

		EstimateBeta.SetupConnection();

		
		Random rnd = new Random();
		for (int i = 0; i < NUMBER_OF_SIMULATION; i++){
			this.simulationNumber = i;
			
			BigDecimal CustKey = new BigDecimal(rnd.nextInt(150000) + 1);
			keys[i] = CustKey;
			
			System.out.println(i + ": Cust Key to find: " + CustKey);
			
			CleanBuffers();
			PointQuery(CustKey, true);
			CleanBuffers();
			PointQuery(CustKey, false);
			CleanBuffers();
			EncryptedNonDet(CustKey);
			
		}


		con.close();
	}
	
	
	
	

	private ArrayList<Customer> PointQuery(BigDecimal custKey, boolean isEncrypted) throws SQLException {
		
		String tableName = "";
		long[] timing = null;
		if(isEncrypted == true){
			tableName = this.encryptedDet;
			timing  = this.timeEncryptedDet;
		}else{
			tableName = this.plainTextTable;
			timing = this.timePlainText;
		}
		
		ArrayList<Customer> CustomerList = new ArrayList<Customer>(50);
		
		String sql = "SELECT C_CUSTKEY , C_NAME , C_ACCTBAL FROM " + tableName + " WHERE C_CUSTKEY = ? ";
		
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setBigDecimal(1, custKey);
		
		long startTime = System.nanoTime();
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()){
			CustomerList.add(new Customer(rs.getBigDecimal(1), rs.getString(2), rs.getBigDecimal(3)));
		}
		
		ps.close();
		
		long stopTime = System.nanoTime();
		
		timing[simulationNumber] += (stopTime - startTime);
		
		return CustomerList;
		
	}





	private void EncryptedNonDet(BigDecimal KeyToSearch) throws SQLException {
		ArrayList<java.sql.RowId> ridsToFetch = GetRIDFullEncryptedTable(KeyToSearch);
		GetTuplesFromFullEncryptedTableByRID(ridsToFetch);
	}

	private ArrayList<Customer> GetTuplesFromFullEncryptedTableByRID(ArrayList<java.sql.RowId> allRIDs)
			throws SQLException {

		if (allRIDs == null || allRIDs.size() == 0) {
			return null;
		}

		String PartialQuery = "SELECT C_CUSTKEY , C_NAME , C_ACCTBAL FROM " + this.encryptedNonDet + " WHERE ";

		ArrayList<Customer> CustomerList = new ArrayList<Customer>(50);

		int ParamCount = allRIDs.size();

		String PlaceHolder = "?";
		for (int i = 0; i < ParamCount; i++) {
			if (i == 0)
				PartialQuery += "ROWID = " + PlaceHolder + " ";
			else
				PartialQuery += "OR ROWID = " + PlaceHolder + " ";

		}

		PreparedStatement ps = con.prepareStatement(PartialQuery);

		for (int i = 0; i < ParamCount; i++) {
			ps.setRowId(i + 1, allRIDs.get(i));// parameter index start from 1
		}

		long startTime = System.nanoTime();
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			CustomerList.add(new Customer(rs.getBigDecimal(1), rs.getString(2), rs.getBigDecimal(3)));
		}
		long stopTime = System.nanoTime();
		timeEncryptedNonDet[simulationNumber] += (stopTime - startTime);

		ps.close();

		return CustomerList;

	}

	private ArrayList<java.sql.RowId> GetRIDFullEncryptedTable(BigDecimal KeyToSearch) throws SQLException {

		ArrayList<java.sql.RowId> RIDList = new ArrayList<java.sql.RowId>(50);

		String sql = " SELECT ROWID,C_CUSTKEY FROM " + this.encryptedNonDet;

		PreparedStatement ps = con.prepareStatement(sql);

		long startTime = System.nanoTime();
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {

			if (rs.getBigDecimal(2).equals(KeyToSearch)) {
				RIDList.add(rs.getRowId(1));
			}

		}

		long stopTime = System.nanoTime();
		timeEncryptedNonDet[simulationNumber] += (stopTime - startTime);

		ps.close();
		return RIDList;

	}

	private static Connection con = null;
	private static String oracleURL = "jdbc:oracle:thin:@localhost:1521/tpch.sensoria.local";

	public static void SetupConnection() throws SQLException {
		DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
		con = DriverManager.getConnection(oracleURL, "tpch_user", "abcdef");

	}

	private void CleanBuffers() throws SQLException {
		PreparedStatement stmt = null;
		try {
			stmt = con.prepareStatement("alter system flush buffer_cache");
			stmt.executeQuery();
			System.out.println("buffer cache flushed");
		} catch (SQLException e) {
			System.out.println("Error clearing buffer cache");
			System.out.println(e.getMessage());
		} finally {
			stmt.close();
		}
	}
	
	private double calculateMean(long[] arr){
		
		double sum = 0;
		for (long l : arr) {
			sum += l;
		}
		return sum / arr.length;
	}
	
}

class Customer {
	public BigDecimal custKey;
	public String name;
	public BigDecimal acctBal;

	public Customer(BigDecimal custKey, String name, BigDecimal acctBal) {
		this.custKey = custKey;
		this.name = name;
		this.acctBal = acctBal;
	}

	public Customer() {
		super();

	}

}

class CustomerOracleStructure {
	public java.sql.RowId rowid;
	public BigDecimal custKey;
	public String name;
	public BigDecimal acctBal;

	public CustomerOracleStructure(RowId rowid, BigDecimal custKey, String name, BigDecimal acctBal) {
		this.rowid = rowid;
		this.custKey = custKey;
		this.name = name;
		this.acctBal = acctBal;
	}

	public CustomerOracleStructure(BigDecimal custKey, String name, BigDecimal acctBal) {
		this.custKey = custKey;
		this.name = name;
		this.acctBal = acctBal;
	}

	public CustomerOracleStructure() {
		super();
	}

}