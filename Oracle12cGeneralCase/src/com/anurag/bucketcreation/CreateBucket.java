package com.anurag.bucketcreation;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.RandomStringUtils;

import com.anurag.utils.Factors;
import com.anurag.utils.MathUtils;


public class CreateBucket {
	
	
	private String sensitiveTableName = ""; //GEN_CUST_10_S
	private String nonSensitiveTableName = "";//GEN_CUST_90_NS
	private int Y = -1; //250
	private int X = -1; //380
	
	private boolean nsIsLarger = true;
	
	
	public final void StartCreatingBuckets(String sensitiveTableName,String nonSensitiveTableName) throws Exception{
		this.sensitiveTableName = sensitiveTableName;
		this.nonSensitiveTableName = nonSensitiveTableName;
		int nsValCount = getNSValueCount();
		int sValCount = getSValueCount();
		if(nsValCount > sValCount){
			this.nsIsLarger = true;
		}else{
			this.nsIsLarger = false;
		}
		
		BufferedWriter bw = new BufferedWriter(new FileWriter("Factors.txt",true));
		
		if(this.nsIsLarger ==true){
			Factors factors = MathUtils.SmallestDiffFactors(nsValCount);
			{
				double cost1 = sValCount/(double)factors.x + factors.x;
				int x = (int)Math.sqrt(nsValCount);
				
				double cost3 = sValCount/(double)(x+1) + (x+1);
				bw.write(new Date().getTime()+"\n");
				bw.write("Sensitive Value Count:"+sValCount + "\t"+"NonSensitive Value Count:"+ nsValCount+"\n");
				bw.write(cost1+" : "+cost3);
				bw.write("\n\n");
				if(cost1 < cost3){
					this.Y = factors.y;
					this.X = factors.x;
				}else{
					this.Y = x+1;
					this.X = x+1;
				}
				
			}
		}else{
			// in this scenario y is the enforcer
            //it forces that max y values can be in each bucket for S
            //and that a minimum of y buckets are created in NS side
            Factors factors = MathUtils.SmallestDiffFactors(sValCount);
            {
                double cost1 =factors.y + (double)nsValCount/(double)factors.y;
                int y = (int)Math.sqrt(sValCount);
                double cost2 = y + (double)nsValCount / (double)y;
                //double cost3 = sValCount / (double)(x + 1) + (x + 1);
                bw.write(new Date().getTime()+"\n");
				bw.write("Sensitive Value Count:"+sValCount + "\t"+"NonSensitive Value Count:"+ nsValCount+"\n");
				bw.write(cost1+" : "+cost2);
				bw.write("\n\n");

                if (cost1 < cost2){
                    this.Y = (int)factors.y;
                    this.X = (int)factors.x;
                }else{
                    this.Y = y;
                    this.X = y;
                }
            }
		}
		

		handleSensitiveKeys();
		handleNonSensitiveKeys();
		normalizeSensitiveBuckets();
		SerializeStuff();
		
		bw.flush();
		bw.close();

	}
	

	private BigDecimal NextFakeKey = new BigDecimal(-1);
	private HashMap<BigDecimal, Integer> SKeyBucketDict = new HashMap<BigDecimal, Integer>();
	private ArrayList<ArrayList<BigDecimal>> SKeyBucketsList = new ArrayList<ArrayList<BigDecimal>>();
	private TreeMap<Integer, Integer> SizeOfSensBuckets_Keys = new TreeMap<Integer, Integer>();
	private HashMap<BigDecimal, Integer> NSKeyBucketDict = new HashMap<BigDecimal, Integer>();
	private ArrayList<ArrayList<BigDecimal>> NSKeyBucketsList = new ArrayList<ArrayList<BigDecimal>>();
	private ArrayList<Integer> SizeOfNonSensBuckets_Values = new ArrayList<Integer>();
	
	
	
	private void SerializeStuff() throws FileNotFoundException, IOException
	{
		SerializeData(SKeyBucketDict, "SKeyBucketDict_" + sensitiveTableName + ".data");
		SerializeData(SKeyBucketsList, "SKeyBuckets_" + sensitiveTableName + ".data");
		SerializeData(NSKeyBucketDict, "NSKeyBucketDict_" + nonSensitiveTableName + ".data");
		SerializeData(NSKeyBucketsList, "NSKeyBuckets_" + nonSensitiveTableName + ".data");
	}
	
	private void SerializeData(Object obj, String FileName) throws FileNotFoundException, IOException
	{
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(FileName));
		oos.writeObject(obj);
		oos.close();
		
	}
	
	private void normalizeSensitiveBuckets() throws SQLException
    {
        //find the bucket which has the max no of keys not values
        int Max = -1;
        Set<Integer> keys= SizeOfSensBuckets_Keys.keySet();
        for(int key : keys)
        {
            int v = SizeOfSensBuckets_Keys.get(key);
            if(v > Max)
            {
                Max = v;
                
            }
        }


        //go through each bucket and insert random stuff in
        // S table,  the dict. SKeyBucketDict and the list SKeyBucketsList
        ArrayList<BigDecimal> ListOfFakeKeysAdded = new ArrayList<BigDecimal>();
        for(int i=0; i<SKeyBucketsList.size(); i++)
        {
            while(SizeOfSensBuckets_Keys.get(i) != Max)
            {
                SKeyBucketDict.put(NextFakeKey, i);
                SKeyBucketsList.get(i).add(NextFakeKey);
                ListOfFakeKeysAdded.add(NextFakeKey);
                NextFakeKey = NextFakeKey.subtract(new BigDecimal(1));
                //SizeOfSensBuckets_Keys[i] += 1; //c# syntax
                int oldVal = SizeOfSensBuckets_Keys.get(i);
                int newVal = oldVal + 1;
                SizeOfSensBuckets_Keys.put(i, newVal);
                
            }
        }

        insertIntoSensitiveTable(ListOfFakeKeysAdded,  sensitiveTableName);
        

    }
	
	
	
	private void insertIntoSensitiveTable(ArrayList<java.math.BigDecimal> FakeKeys, String TableName) throws SQLException
	{

		
		String InsertStatement = 
						"INSERT INTO "+TableName+  
						" ( C_CUSTKEY , C_NAME, C_ADDRESS, C_NATIONKEY , C_PHONE, C_ACCTBAL, C_MKTSEGMENT, C_COMMENT ) " + 
						"VALUES " + 
						" ( ? , ? , ? , ? , ? , ? , ?, ? )  ";

		PreparedStatement stmt = con.prepareStatement(InsertStatement);
		
		for(BigDecimal Akey: FakeKeys)
		{
			stmt.setBigDecimal(1, Akey);
			stmt.setString(2,RandomString(25));
			stmt.setString(3,RandomString(40));
			stmt.setBigDecimal(4, Akey);
			stmt.setString(5,RandomString(15));
			stmt.setBigDecimal(6, Akey);
			stmt.setString(7,RandomString(10));
			stmt.setString(8,RandomString(100));
			
			stmt.addBatch();
		}
		
		int[] affctedRowsArr = stmt.executeBatch();
		stmt.close();
		
	}
	
	
	
	
	private void handleNonSensitiveKeys() throws SQLException
	{
		ArrayList<BigDecimal> NSKeys = getAllCustKeyFromNonSensitive();
		Collections.sort(NSKeys);
		fillUpDictAndBucketsForNonSensitive(NSKeys);

	}
	
	
	
	private void fillUpDictAndBucketsForNonSensitive(ArrayList<java.math.BigDecimal> NSKeys)
	{

		int BucketSize = -1;
		int NumberOfBuckets = -1;
		
		if(this.nsIsLarger == true){
			
			BucketSize = this.X;
			NumberOfBuckets = (int)Math.ceil(
					(double)(new HashSet<BigDecimal>(NSKeys)).size() / 
					(double)BucketSize); //no of distinct values / bucket size
		}else{
			//case when S is larger, we are fixing the number of buckets as Y
            //bucket size is the dependent value
            NumberOfBuckets = this.Y;
            BucketSize = (int)Math.ceil(
                (double)new HashSet<BigDecimal>(NSKeys).size() /
                (double)NumberOfBuckets);
		}
		
		
		
		for (int i = 0; i < NumberOfBuckets; i++)
		{
			NSKeyBucketsList.add(new ArrayList<BigDecimal>());
			SizeOfNonSensBuckets_Values.add(0);
		}

		for(BigDecimal AKey : NSKeys)
        {
            //check if a corresponding key exist in S side
            int BucketIndexFound = -1;
            if(SKeyBucketDict.containsKey(AKey)){
            	BucketIndexFound = SKeyBucketDict.get(AKey);
            	//check if the key is already added to NS buckets by looking up in NS dictionary
                if(NSKeyBucketDict.containsKey(AKey) == true)
                {
                    //the key has already been processed
                }
                else
                {
                    //find out the position of the key inside a bucket in the S side
                    int TargetBucket = SKeyBucketsList.get(BucketIndexFound).indexOf(AKey);
                    NSKeyBucketDict.put(AKey, TargetBucket);
                    NSKeyBucketsList.get(TargetBucket).add(AKey);
                    Integer oldVal = SizeOfNonSensBuckets_Values.get(TargetBucket);//+1
                    Integer newVal = oldVal + 1;
                    SizeOfNonSensBuckets_Values.set(TargetBucket, newVal);
                }

            }else{
            	//ignore in the first pass
            }
            

		}
		
		//2nd pass
		int NSBucket = 0;
        for(NSBucket = 0; NSBucket < SizeOfNonSensBuckets_Values.size(); NSBucket++)
        {
            if (SizeOfNonSensBuckets_Values.get(NSBucket) < BucketSize)
                break;
        }
        for (BigDecimal AKey : NSKeys)
        {
            
            //check if the key is already present in the dictionary
            if (NSKeyBucketDict.containsKey(AKey) == true)
            {
                //this means that the key has already been processed in the first pass
                //or its a duplicate key processed in the second pass
                
            }
            else
            {
                NSKeyBucketDict.put(AKey, NSBucket);
                NSKeyBucketsList.get(NSBucket).add(AKey);
                //SizeOfNonSensBuckets_Values[NSBucket] += 1;//c# code
                int oldVal=SizeOfNonSensBuckets_Values.get(NSBucket);
                int newVal=oldVal+1;
                SizeOfNonSensBuckets_Values.set(NSBucket, newVal);

                //get the next empty bucket
                for (; NSBucket < SizeOfNonSensBuckets_Values.size(); NSBucket++)
                {
                    if (SizeOfNonSensBuckets_Values.get(NSBucket) < BucketSize)
                        break;
                }
            }

        }
		
	}
	
	
	
	private ArrayList<java.math.BigDecimal> getAllCustKeyFromNonSensitive() throws SQLException
	{
		ArrayList<BigDecimal> allCustKeys = new ArrayList<BigDecimal>();

		String sql = "SELECT C_CUSTKEY FROM  "+ nonSensitiveTableName +" ";
		
		PreparedStatement stmt = con.prepareStatement(sql);
		
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()){
			allCustKeys.add(rs.getBigDecimal(1));
		}
	
		rs.close();
		stmt.close();

		return allCustKeys;


	}
	
	
	/*
	 * Optimization added as per Prof. Jeff
	 */
	private void handleSensitiveKeys() throws SQLException{
		ArrayList<CustKeyAndCount> SKeys = getAllCustKeyWithCountFromSensitive();
		fillUpDictAndBucketsForSensitive(SKeys);
	}
	
	
	/*
	 * Optimization added as per Prof. Jeff
	 */
	private void fillUpDictAndBucketsForSensitive(ArrayList<CustKeyAndCount> Keys)
	{
		
		int numberOfSensitiveBuckets = -1;
		
		if(this.nsIsLarger == true){
			numberOfSensitiveBuckets = this.X;
		}else{
			numberOfSensitiveBuckets = (int)Math.ceil((double)Keys.size() / (double)this.Y);
            //number of values in sensitive / max allowed per bucket which is Y
		}
		
		
		//create the buckets
		for(int i=0 ; i<numberOfSensitiveBuckets ; i++){
			SKeyBucketsList.add(new ArrayList<BigDecimal>());
			SizeOfSensBuckets_Keys.put(i, 0);
		}
		
		
		
		for(CustKeyAndCount aKey : Keys){
			int k = findAppropriateBucket();
			SKeyBucketDict.put(aKey.key, k);
			SKeyBucketsList.get(k).add(aKey.key);
			
			int currSizeOfBucket = SizeOfSensBuckets_Keys.get(k);
			currSizeOfBucket += aKey.count;
			SizeOfSensBuckets_Keys.put(k, currSizeOfBucket);
					
		}
		
		
	}
	
	
	private int findAppropriateBucket() {
		//find the bucket which has the least number of keys
		int min = Integer.MAX_VALUE;
		int minIndex = -1;
		int numberOfValuesPerBucket = this.Y;
		//this.Y remains same whether we have ns > s or ns<s
        //in the later this is actually enforced
		
		Iterator<Entry<Integer, Integer>> itr = SizeOfSensBuckets_Keys.entrySet().iterator();
		while(itr.hasNext()){
			Entry<Integer, Integer> t = itr.next();
			if(
					t.getValue() < min && //update if we have found the new minimum
					SKeyBucketsList.get(t.getKey()).size() < numberOfValuesPerBucket //check that we have not crossed the values limit per bucket
			){
				min = t.getValue();
				minIndex = t.getKey();
			}
		}
		
		return minIndex;
	}

	/*
	 * The keys are sorted desc. by count
	 */
	private ArrayList<CustKeyAndCount> getAllCustKeyWithCountFromSensitive() throws SQLException{
		ArrayList<CustKeyAndCount> allCustKeys = new ArrayList<CustKeyAndCount>();

		String sql = "SELECT VAL, CNT FROM ( "+
					 "SELECT C_CUSTKEY AS VAL , COUNT(C_CUSTKEY) AS CNT FROM " + this.sensitiveTableName +
					 " GROUP BY C_CUSTKEY) "+
					 " ORDER BY CNT DESC ";
		
		PreparedStatement stmt = con.prepareStatement(sql);
		
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()){
			allCustKeys.add(new CustKeyAndCount(rs.getBigDecimal(1), rs.getLong(2)));
		}
	
		rs.close();
		stmt.close();

		return allCustKeys;


	}
	
	
	private static Connection con= null;
	private static String oracleURL = "jdbc:oracle:thin:@localhost:1521/tpch.sensoria.local";
    public static void SetupConnection() throws SQLException{
    	DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
    	con = DriverManager.getConnection(oracleURL, "tpch_user", "abcdef");

    }
    
    public static void closeConnection() throws SQLException{
    	con.close();
    }
    
    
    private final String RandomString(int length)
	{

		return RandomStringUtils.random(length);
	}
    
    private int getNSValueCount() throws SQLException
    {
    	
    	long distVals = -1;
    	String sql = "SELECT COUNT(DISTINCT(C_CUSTKEY)) FROM "+ nonSensitiveTableName;

    	PreparedStatement stmt = con.prepareStatement(sql);
    	
    	ResultSet rs = stmt.executeQuery();
    	
    	if(rs.next()){
    		 distVals = rs.getLong(1);
    	}
    	rs.close();
    	stmt.close();
    	//TODO: Forced conversion not good
    	return (int)distVals;
    }
    
    
    private int getSValueCount() throws SQLException
    {
    	
    	long distVals = -1;
    	String sql = "SELECT COUNT(DISTINCT(C_CUSTKEY)) FROM "+ sensitiveTableName;

    	PreparedStatement stmt = con.prepareStatement(sql);
    	
    	ResultSet rs = stmt.executeQuery();
    	
    	if(rs.next()){
    		 distVals = rs.getLong(1);
    	}
    	rs.close();
    	stmt.close();
    	//TODO: Forced conversion not good
    	return (int)distVals;
    }
    


}

class CustKeyAndCount{
	public BigDecimal key;
	public long count;
	public CustKeyAndCount(BigDecimal key, long count) {
		this.key = key;
		this.count = count;
	}
	public CustKeyAndCount() {
		
	}
	
}
