package com.anurag;



import java.sql.SQLException;

import com.anurag.bucketcreation.CreateBucket;
import com.anurag.simulation.SimulationTimings;

public class DriverProgram {

	
	public static void main(String[] args) throws Exception {
		bucketCreationStage();
		
		//simulationStage();
		
	}

	
	
	private static void simulationStage() throws Exception{
		SimulationTimings.SetupConnection();
		SimulationTimings st = null;
		
		st= new SimulationTimings();
		st.perform("GEN_CUST_10_S", "GEN_CUST_90_NS", "CUSTOMER_ALL_ENCRYPTED", 10);
		
		st= new SimulationTimings();
		st.perform("GEN_CUST_20_S", "GEN_CUST_80_NS", "CUSTOMER_ALL_ENCRYPTED", 20);
		
		st= new SimulationTimings();
		st.perform("GEN_CUST_30_S", "GEN_CUST_70_NS", "CUSTOMER_ALL_ENCRYPTED", 30);
		
		st= new SimulationTimings();
		st.perform("GEN_CUST_40_S", "GEN_CUST_60_NS", "CUSTOMER_ALL_ENCRYPTED", 40);
		
		st= new SimulationTimings();
		st.perform("GEN_CUST_50_S", "GEN_CUST_50_NS", "CUSTOMER_ALL_ENCRYPTED", 50);
//
//		st= new SimulationTimings();
//		st.perform("GEN_CUST_60_S", "GEN_CUST_40_NS", "CUSTOMER_ALL_ENCRYPTED", 60);
//		
//		st= new SimulationTimings();
//		st.perform("GEN_CUST_70_S", "GEN_CUST_30_NS", "CUSTOMER_ALL_ENCRYPTED", 70);
//		
//		st= new SimulationTimings();
//		st.perform("GEN_CUST_80_S", "GEN_CUST_20_NS", "CUSTOMER_ALL_ENCRYPTED", 80);
		
		SimulationTimings.closeConnection();
		
		
	}
	
	
	private static void bucketCreationStage() throws SQLException, Exception {
		CreateBucket.SetupConnection();
		
		
		CreateBucket cb = null;
		
		
//		cb=new CreateBucket();
//		cb.StartCreatingBuckets("GEN_CUST_10_S", "GEN_CUST_90_NS");
//		
//		cb=new CreateBucket();
//		cb.StartCreatingBuckets("GEN_CUST_20_S", "GEN_CUST_80_NS");
//		
//		cb=new CreateBucket();
//		cb.StartCreatingBuckets("GEN_CUST_30_S", "GEN_CUST_70_NS");
//		
//		cb=new CreateBucket();
//		cb.StartCreatingBuckets("GEN_CUST_40_S", "GEN_CUST_60_NS");
//		
//		cb=new CreateBucket();
//		cb.StartCreatingBuckets("GEN_CUST_50_S", "GEN_CUST_50_NS");
		
		cb=new CreateBucket();
		cb.StartCreatingBuckets("GEN_CUST_60_S", "GEN_CUST_40_NS");
		
		cb=new CreateBucket();
		cb.StartCreatingBuckets("GEN_CUST_70_S", "GEN_CUST_30_NS");
		
		cb=new CreateBucket();
		cb.StartCreatingBuckets("GEN_CUST_80_S", "GEN_CUST_20_NS");
		
		cb=new CreateBucket();
		cb.StartCreatingBuckets("GEN_CUST_90_S", "GEN_CUST_10_NS");
		

		
		CreateBucket.closeConnection();
	}
}
