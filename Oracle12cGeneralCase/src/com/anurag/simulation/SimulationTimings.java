package com.anurag.simulation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;





public class SimulationTimings {
	
	
	
	private long[] ServerTimes = new long[NUMBER_OF_SIMULATIONS];
	private long[] ClientTimes = new long[NUMBER_OF_SIMULATIONS];
	private long[] NonEncryptedTimes = new long[NUMBER_OF_SIMULATIONS];
	private long[] NoBuckets_Server = new long[NUMBER_OF_SIMULATIONS];
	private long[] NoBuckets_Client = new long[NUMBER_OF_SIMULATIONS];
	private BigDecimal[] Keys = new BigDecimal[NUMBER_OF_SIMULATIONS];

	private HashMap<BigDecimal, Integer> NSKeyBucketDict;
	private ArrayList<ArrayList<BigDecimal>> NSKeyBuckets;
	private HashMap<BigDecimal, Integer> SKeyBucketDict;
	private ArrayList<ArrayList<BigDecimal>> SKeyBuckets;

	
	private String sensitiveTableName = "";
	private String nonSensitiveTableName = "";
	private String fullTableName = "";
	private int unit = -1;
	private String ResultsFileName = "";
	private boolean FoundInSensitive = false;

	private static final int NUMBER_OF_SIMULATIONS = 50;
	private int simulationNumber = 0;
	private long startTime;
	private long stopTime;
	
	public final void perform(String sensitiveTableName, String nonSensitiveTableName, String fullTableName, int unit) throws Exception
	{
		
		System.out.println("Started for "+unit);
		this.sensitiveTableName = sensitiveTableName;
		this.nonSensitiveTableName = nonSensitiveTableName;
		this.fullTableName = fullTableName;
		this.unit = unit;
		
		
		this.ResultsFileName = "Timing_" + this.unit + ".tsv";

		Random rnd = new Random();

		LoadBucketsFromFile();
		for (int i = 0; i < NUMBER_OF_SIMULATIONS; i++)
		{
			simulationNumber = i;
			
			//System.out.println("Enter the Cutomer key to Search");
			//decimal CustKey = Convert.ToDecimal(Console.ReadLine());

			java.math.BigDecimal CustKey = new BigDecimal(rnd.nextInt(150000) + 1);
			Keys[i]=CustKey;
			System.out.println(i + ": Cust Key to find: " + CustKey);
			if (SKeyBucketDict.containsKey(CustKey))
			{
				//the key exists in sensitive bucket.
				//so the bucket number is fixed for both sensitive and non sensitive side
				this.FoundInSensitive = true;

			}
			else
			{
				this.FoundInSensitive = false;

			}
			CleanBuffers();
			FetchSensitiveTuples(CustKey);
			FetchNonSensitiveTuples(CustKey);
			SelectCustomerNonBucketization(CustKey);
			
		}
		PrintTimings();

	}
	
	



	private void PrintTimings() throws IOException
    {
        System.out.println("Writing The Timings to file:" + this.ResultsFileName);
        
        BufferedWriter file = new BufferedWriter(new FileWriter(new File(this.ResultsFileName)));
        {
            
        	file.write("Key\tServer\tClient\tNonSens.\tNoBuckets");
        	file.newLine();
            for (int i = 0; i < NUMBER_OF_SIMULATIONS; i++)
            {
                file.write(
                    Keys[i] + "\t" +
                    ServerTimes[i] + "\t" +
                    ClientTimes[i] + "\t" +
                    NonEncryptedTimes[i] + "\t" +
                    NoBuckets_Server[i] + "\t" +
                    NoBuckets_Client[i] + "\n");
            }
            file.newLine();
            file.newLine();
            file.newLine();
            file.write(
                calculateMean(ServerTimes) + "\t" +
                calculateMean(ClientTimes) + "\t" +
                calculateMean(NonEncryptedTimes) + "\t" +
                calculateMean(NoBuckets_Server) + "\t" +
                calculateMean(NoBuckets_Client)
                );
        }
        file.flush();
        file.close();
        System.out.println("Writing Finished....");

    }
	
	
	private void SelectCustomerNonBucketization(BigDecimal KeyToSearch) throws SQLException {
		ArrayList<java.sql.RowId> ridsToFetch = GetRIDFullEncryptedTable(KeyToSearch);
		GetTuplesFromFullEncryptedTableByRID(ridsToFetch);
	}
	
	
	private ArrayList<Customer> GetTuplesFromFullEncryptedTableByRID(ArrayList<java.sql.RowId> allRIDs) throws SQLException
    {

        
		if(allRIDs== null || allRIDs.size() == 0){
			return null;
		}
		
		String PartialQuery =
        "SELECT C_CUSTKEY , C_NAME , C_ACCTBAL FROM "+ this.fullTableName+ " WHERE ";
        


        ArrayList<Customer> CustomerList = new ArrayList<Customer>();
        

        int ParamCount = allRIDs.size();


        String PlaceHolder = "?";
        for (int i = 0; i < ParamCount; i++)
        {
            if (i == 0)
                PartialQuery += "ROWID = " + PlaceHolder  + " ";
            else
                PartialQuery += "OR ROWID = " + PlaceHolder + " ";

        }
        
        
        PreparedStatement ps = con.prepareStatement(PartialQuery);
        
        for(int i =0 ; i<ParamCount ; i++){
        	ps.setRowId(i+1, allRIDs.get(i));//parameter index start from 1
        }
        
        startTime = System.nanoTime();
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
        	CustomerList.add(new Customer(
        			rs.getBigDecimal(1),
        			rs.getString(2),
        			rs.getBigDecimal(3)
        			));
        }
        stopTime = System.nanoTime();
        NoBuckets_Server[simulationNumber] += (stopTime-startTime);
        
        ps.close();
        
        return CustomerList;

        
    }
	
	
	private ArrayList<java.sql.RowId> GetRIDFullEncryptedTable(BigDecimal KeyToSearch) throws SQLException
    {

        ArrayList<java.sql.RowId> RIDList = new ArrayList<java.sql.RowId>();
        ArrayList<CustomerOracleStructure> custTempList = new ArrayList<CustomerOracleStructure>();
        String sql = " SELECT ROWID,C_CUSTKEY FROM "+ this.fullTableName ;
        
        PreparedStatement ps = con.prepareStatement(sql);
        
        startTime = System.nanoTime();
        ResultSet rs= ps.executeQuery();
        
        while(rs.next())
        {
        	CustomerOracleStructure cust = new CustomerOracleStructure();
        	cust.rowid = rs.getRowId(1);
        	cust.custKey = rs.getBigDecimal(2);
        	custTempList.add(cust);
        }
        
        stopTime = System.nanoTime();
        NoBuckets_Server[simulationNumber] += (stopTime-startTime);
        
        startTime = System.nanoTime();
        for (CustomerOracleStructure t : custTempList) {
			if(t.custKey.equals(KeyToSearch)){
				RIDList.add(t.rowid);
			}
		}
        stopTime = System.nanoTime();
        NoBuckets_Client[simulationNumber] += (stopTime - startTime);
        
        ps.close();
        return RIDList;


    }
	
	
	
    private void FetchNonSensitiveTuples(BigDecimal key) throws SQLException
    {

        Integer BucketNumber = -1;
        if (this.FoundInSensitive == true)
        {
            int SBucketNumber = SKeyBucketDict.get(key);
            int PosnInSBucket = SKeyBuckets.get(SBucketNumber).indexOf(key);
            BucketNumber = PosnInSBucket;
        }
        else
        {
            //It is possible that key is neither present in S nor in NS
        	BucketNumber = NSKeyBucketDict.get(key) ;
        	if(BucketNumber == null)
            {
                //get some random bucket
                BucketNumber = new Random().nextInt(NSKeyBuckets.size());
            }
        }

        ArrayList<BigDecimal> AllKeys = NSKeyBuckets.get(BucketNumber);
        ArrayList<Customer> AllCustomers = SelectCustomersByIds(AllKeys);
    }
	
	
	ArrayList<Customer> SelectCustomersByIds(ArrayList<BigDecimal> CustKeys) throws SQLException
    {

        String PartialQuery =
            "SELECT C_CUSTKEY, C_NAME, C_ACCTBAL FROM "+ nonSensitiveTableName+" WHERE ";
        




        ArrayList<Customer> CustomerList = new ArrayList<Customer>();
       

        int KeyCount = CustKeys.size();


        String PlaceHolder = "?";
        for (int i = 0; i < KeyCount; i++)
        {
            if (i == 0)
                PartialQuery += "C_CUSTKEY = " + PlaceHolder  + " ";
            else
                PartialQuery += "OR C_CUSTKEY = " + PlaceHolder  + " ";

        }
        
        PreparedStatement ps = con.prepareStatement(PartialQuery);
        for (int i = 0; i < KeyCount; i++)
        {
        	ps.setBigDecimal(i+1, CustKeys.get(i));
        }
        
        
        startTime = System.nanoTime();
        ResultSet rs = ps.executeQuery();
        
        while(rs.next()){
        	CustomerList.add(
        			new Customer(
        					rs.getBigDecimal(1),rs.getString(2),rs.getBigDecimal(3)
        					));
        }
        stopTime = System.nanoTime();
        NonEncryptedTimes[simulationNumber] += (stopTime - startTime);
        
        ps.close();
        
        return CustomerList;

        

        
    }
	
    private void FetchSensitiveTuples(BigDecimal key) throws SQLException
    {
        int BucketNumber = -1;

        if (this.FoundInSensitive == true)
        {
            BucketNumber = SKeyBucketDict.get(key);
        }
        else
        {
            BucketNumber = new Random().nextInt(SKeyBuckets.size());
        }
        ArrayList<BigDecimal> AllKeys = SKeyBuckets.get(BucketNumber);
        ArrayList<java.sql.RowId> AllRIDs = GetRIDFromSensitive(new HashSet<BigDecimal>(AllKeys));
        GetTuplesFromSensitiveByRID(AllRIDs);


    }
	
	private ArrayList<Customer> GetTuplesFromSensitiveByRID(ArrayList<java.sql.RowId> allRIDs) throws SQLException
    {

        
		String PartialQuery =
        "SELECT C_CUSTKEY , C_NAME , C_ACCTBAL FROM "+ sensitiveTableName+ " WHERE ";
        


        ArrayList<Customer> CustomerList = new ArrayList<Customer>();
        

        int ParamCount = allRIDs.size();


        String PlaceHolder = "?";
        for (int i = 0; i < ParamCount; i++)
        {
            if (i == 0)
                PartialQuery += "ROWID = " + PlaceHolder  + " ";
            else
                PartialQuery += "OR ROWID = " + PlaceHolder + " ";

        }
        
        
        PreparedStatement ps = con.prepareStatement(PartialQuery);
        
        for(int i =0 ; i<ParamCount ; i++){
        	ps.setRowId(i+1, allRIDs.get(i));//parameter index start from 1
        }
        
        startTime = System.nanoTime();
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
        	CustomerList.add(new Customer(
        			rs.getBigDecimal(1),
        			rs.getString(2),
        			rs.getBigDecimal(3)
        			));
        }
        stopTime = System.nanoTime();
        ServerTimes[simulationNumber] += (stopTime-startTime);
        
        ps.close();
        
        return CustomerList;

        
    }
	
	
	private ArrayList<java.sql.RowId> GetRIDFromSensitive(HashSet<BigDecimal> CustKeySet) throws SQLException
    {

        ArrayList<java.sql.RowId> RIDList = new ArrayList<java.sql.RowId>();
        ArrayList<CustomerOracleStructure> custTempList = new ArrayList<CustomerOracleStructure>();
        String sql = " SELECT ROWID,C_CUSTKEY FROM "+ sensitiveTableName ;
        
        PreparedStatement ps = con.prepareStatement(sql);
        
        startTime = System.nanoTime();
        ResultSet rs= ps.executeQuery();
        
        while(rs.next())
        {
        	CustomerOracleStructure cust = new CustomerOracleStructure();
        	cust.rowid = rs.getRowId(1);
        	cust.custKey = rs.getBigDecimal(2);
        	custTempList.add(cust);
        }
        
        stopTime = System.nanoTime();
        ServerTimes[simulationNumber] += (stopTime-startTime);
        
        startTime = System.nanoTime();
        for (CustomerOracleStructure t : custTempList) {
			if(CustKeySet.contains(t.custKey)){
				RIDList.add(t.rowid);
			}
		}
        stopTime = System.nanoTime();
        ClientTimes[simulationNumber] += (stopTime - startTime);
        
        ps.close();
        return RIDList;


    }
	

	
	@SuppressWarnings("unchecked")
	private void LoadBucketsFromFile() throws FileNotFoundException, ClassNotFoundException, IOException
	{
		SKeyBucketDict = (HashMap<BigDecimal, Integer>)Deserialize("SKeyBucketDict_" + sensitiveTableName + ".data");
		SKeyBuckets = (ArrayList<ArrayList<BigDecimal>>)Deserialize("SKeyBuckets_" + sensitiveTableName + ".data");
		NSKeyBucketDict = (HashMap<BigDecimal, Integer>)Deserialize("NSKeyBucketDict_" + nonSensitiveTableName + ".data");
		NSKeyBuckets = (ArrayList<ArrayList<BigDecimal>>)Deserialize("NSKeyBuckets_" + nonSensitiveTableName + ".data");
	}
	
	private Object Deserialize(String FileName) throws FileNotFoundException, IOException, ClassNotFoundException
	{

		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FileName));
		Object obj = ois.readObject();
		ois.close();
		return obj;

	}
	
	private static Connection con= null;
	private static String oracleURL = "jdbc:oracle:thin:@localhost:1521/tpch.sensoria.local";
    public static void SetupConnection() throws SQLException{
    	DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
    	con = DriverManager.getConnection(oracleURL, "tpch_user", "abcdef");

    }
    
    public static void closeConnection() throws SQLException{
    	con.close();
    }
    
	private void CleanBuffers() {
		PreparedStatement stmt = null;
		try {
			stmt = con.prepareStatement("alter system flush buffer_cache");
			stmt.executeQuery();
			System.out.println("buffer cache flushed");
		} catch (SQLException e) {
			System.out.println("Error clearing buffer cache");
			System.out.println(e.getMessage());
		}finally{
			stmt.close();
		}
	}
	
	private double calculateMean(long[] arr){
		
		double sum = 0;
		for (long l : arr) {
			sum += l;
		}
		return sum / arr.length;
	}
	
}

class CustomerOracleStructure
{
	public java.sql.RowId rowid;
	public BigDecimal custKey;
	public String name;
	public BigDecimal acctBal;
	public CustomerOracleStructure(RowId rowid, BigDecimal custKey, String name, BigDecimal acctBal) {
		this.rowid = rowid;
		this.custKey = custKey;
		this.name = name;
		this.acctBal = acctBal;
	}
	
	public CustomerOracleStructure(BigDecimal custKey, String name, BigDecimal acctBal) {
		this.custKey = custKey;
		this.name = name;
		this.acctBal = acctBal;
	}
	public CustomerOracleStructure() {
		super();
	}
	
	
	
}

class Customer
{
	public BigDecimal custKey;
	public String name;
	public BigDecimal acctBal;
	public Customer(BigDecimal custKey, String name, BigDecimal acctBal) {
		this.custKey = custKey;
		this.name = name;
		this.acctBal = acctBal;
	}
	public Customer() {
		super();
		
	}
	
	
}
