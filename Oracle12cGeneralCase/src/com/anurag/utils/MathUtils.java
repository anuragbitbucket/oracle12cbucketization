package com.anurag.utils;

public class MathUtils {

	public static Factors SmallestDiffFactors(int n)
	{
		int p = 1, q = 1;
		int upper = (int)Math.sqrt(n);
		for (int i = upper; i > 1; i--)
		{
			p = i;
			q = n / p;
			if (n == p * q)
			{
				break;
			}
		}
		if (p < q)
		{
			return new Factors(q, p);

		}
		else
		{
			return new Factors(p, q);
		}
	}
}
