declare

  L_SQL1 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_10_S MODIFY (C_CUSTKEY ENCRYPT SALT)';
  L_SQL2 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_10_S MODIFY (C_NAME ENCRYPT SALT)';
  L_SQL3 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_10_S MODIFY (C_ACCTBAL ENCRYPT SALT)';
  
  L_SQL4 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_20_S MODIFY (C_CUSTKEY ENCRYPT SALT)';
  L_SQL5 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_20_S MODIFY (C_NAME ENCRYPT SALT)';
  L_SQL6 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_20_S MODIFY (C_ACCTBAL ENCRYPT SALT)';
  
  L_SQL7 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_30_S MODIFY (C_CUSTKEY ENCRYPT SALT)';
  L_SQL8 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_30_S MODIFY (C_NAME ENCRYPT SALT)';
  L_SQL9 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_30_S MODIFY (C_ACCTBAL ENCRYPT SALT)';
  
  L_SQL10 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_40_S MODIFY (C_CUSTKEY ENCRYPT SALT)';
  L_SQL11 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_40_S MODIFY (C_NAME ENCRYPT SALT)';
  L_SQL12 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_40_S MODIFY (C_ACCTBAL ENCRYPT SALT)';
  
  L_SQL13 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_50_S MODIFY (C_CUSTKEY ENCRYPT SALT)';
  L_SQL14 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_50_S MODIFY (C_NAME ENCRYPT SALT)';
  L_SQL15 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_50_S MODIFY (C_ACCTBAL ENCRYPT SALT)';
  
  L_SQL16 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_60_S MODIFY (C_CUSTKEY ENCRYPT SALT)';
  L_SQL17 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_60_S MODIFY (C_NAME ENCRYPT SALT)';
  L_SQL18 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_60_S MODIFY (C_ACCTBAL ENCRYPT SALT)';
  
  L_SQL19 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_70_S MODIFY (C_CUSTKEY ENCRYPT SALT)';
  L_SQL20 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_70_S MODIFY (C_NAME ENCRYPT SALT)';
  L_SQL21 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_70_S MODIFY (C_ACCTBAL ENCRYPT SALT)';
  
  L_SQL22 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_80_S MODIFY (C_CUSTKEY ENCRYPT SALT)';
  L_SQL23 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_80_S MODIFY (C_NAME ENCRYPT SALT)';
  L_SQL24 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_80_S MODIFY (C_ACCTBAL ENCRYPT SALT)';
  
  L_SQL25 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_90_S MODIFY (C_CUSTKEY ENCRYPT SALT)';
  L_SQL26 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_90_S MODIFY (C_NAME ENCRYPT SALT)';
  L_SQL27 VARCHAR2(200) := 'ALTER TABLE TENX_GEN_CUST_90_S MODIFY (C_ACCTBAL ENCRYPT SALT)';

--CREATE INDEX
  L_SQL50 VARCHAR2(200) := 'CREATE INDEX TENX_CUST_90_NS_IDX ON TENX_GEN_CUST_90_NS(C_CUSTKEY) TABLESPACE USERS ';
  L_SQL51 VARCHAR2(200) := 'CREATE INDEX TENX_CUST_80_NS_IDX ON TENX_GEN_CUST_80_NS(C_CUSTKEY) TABLESPACE USERS ';
  L_SQL52 VARCHAR2(200) := 'CREATE INDEX TENX_CUST_70_NS_IDX ON TENX_GEN_CUST_70_NS(C_CUSTKEY) TABLESPACE USERS ';
  L_SQL53 VARCHAR2(200) := 'CREATE INDEX TENX_CUST_60_NS_IDX ON TENX_GEN_CUST_60_NS(C_CUSTKEY) TABLESPACE USERS ';
  L_SQL54 VARCHAR2(200) := 'CREATE INDEX TENX_CUST_50_NS_IDX ON TENX_GEN_CUST_50_NS(C_CUSTKEY) TABLESPACE USERS ';
  L_SQL55 VARCHAR2(200) := 'CREATE INDEX TENX_CUST_40_NS_IDX ON TENX_GEN_CUST_40_NS(C_CUSTKEY) TABLESPACE USERS ';
  L_SQL56 VARCHAR2(200) := 'CREATE INDEX TENX_CUST_30_NS_IDX ON TENX_GEN_CUST_30_NS(C_CUSTKEY) TABLESPACE USERS ';
  L_SQL57 VARCHAR2(200) := 'CREATE INDEX TENX_CUST_20_NS_IDX ON TENX_GEN_CUST_20_NS(C_CUSTKEY) TABLESPACE USERS ';
  L_SQL58 VARCHAR2(200) := 'CREATE INDEX TENX_CUST_10_NS_IDX ON TENX_GEN_CUST_10_NS(C_CUSTKEY) TABLESPACE USERS ';
  
begin  
  for idx in 1..10 loop
    INSERT INTO TENX_GEN_CUST_10_S SELECT * FROM GEN_CUST_10_S WHERE C_CUSTKEY > 0;
    INSERT INTO TENX_GEN_CUST_20_S SELECT * FROM GEN_CUST_20_S WHERE C_CUSTKEY > 0;
    INSERT INTO TENX_GEN_CUST_30_S SELECT * FROM GEN_CUST_30_S WHERE C_CUSTKEY > 0;
    INSERT INTO TENX_GEN_CUST_40_S SELECT * FROM GEN_CUST_40_S WHERE C_CUSTKEY > 0;
    INSERT INTO TENX_GEN_CUST_50_S SELECT * FROM GEN_CUST_50_S WHERE C_CUSTKEY > 0;
    INSERT INTO TENX_GEN_CUST_60_S SELECT * FROM GEN_CUST_60_S WHERE C_CUSTKEY > 0;
    INSERT INTO TENX_GEN_CUST_70_S SELECT * FROM GEN_CUST_70_S WHERE C_CUSTKEY > 0;
    INSERT INTO TENX_GEN_CUST_80_S SELECT * FROM GEN_CUST_80_S WHERE C_CUSTKEY > 0;
    INSERT INTO TENX_GEN_CUST_90_S SELECT * FROM GEN_CUST_90_S WHERE C_CUSTKEY > 0;
    
    COMMIT;
    
    INSERT INTO TENX_GEN_CUST_90_NS SELECT * FROM GEN_CUST_90_NS;
    INSERT INTO TENX_GEN_CUST_80_NS SELECT * FROM GEN_CUST_80_NS;
    INSERT INTO TENX_GEN_CUST_70_NS SELECT * FROM GEN_CUST_70_NS;
    INSERT INTO TENX_GEN_CUST_60_NS SELECT * FROM GEN_CUST_60_NS;
    INSERT INTO TENX_GEN_CUST_50_NS SELECT * FROM GEN_CUST_50_NS;
    INSERT INTO TENX_GEN_CUST_40_NS SELECT * FROM GEN_CUST_40_NS;
    INSERT INTO TENX_GEN_CUST_30_NS SELECT * FROM GEN_CUST_30_NS;
    INSERT INTO TENX_GEN_CUST_20_NS SELECT * FROM GEN_CUST_20_NS;
    INSERT INTO TENX_GEN_CUST_10_NS SELECT * FROM GEN_CUST_10_NS;
    
    COMMIT;
    dbms_output.put_line ('COMPLETED'||idx);

 end loop;
 
 EXECUTE IMMEDIATE L_SQL1;
 EXECUTE IMMEDIATE L_SQL2;
 EXECUTE IMMEDIATE L_SQL3;
 EXECUTE IMMEDIATE L_SQL4;
 EXECUTE IMMEDIATE L_SQL5;
 EXECUTE IMMEDIATE L_SQL6;
 EXECUTE IMMEDIATE L_SQL7;
 EXECUTE IMMEDIATE L_SQL8;
 EXECUTE IMMEDIATE L_SQL9;
 EXECUTE IMMEDIATE L_SQL10;

 EXECUTE IMMEDIATE L_SQL11;
 EXECUTE IMMEDIATE L_SQL12;
 EXECUTE IMMEDIATE L_SQL13;
 EXECUTE IMMEDIATE L_SQL14;
 EXECUTE IMMEDIATE L_SQL15;
 EXECUTE IMMEDIATE L_SQL16;
 EXECUTE IMMEDIATE L_SQL17;
 EXECUTE IMMEDIATE L_SQL18;
 EXECUTE IMMEDIATE L_SQL19;
 EXECUTE IMMEDIATE L_SQL20;
 
 EXECUTE IMMEDIATE L_SQL21;
 EXECUTE IMMEDIATE L_SQL22;
 EXECUTE IMMEDIATE L_SQL23;
 EXECUTE IMMEDIATE L_SQL24;
 EXECUTE IMMEDIATE L_SQL25;
 EXECUTE IMMEDIATE L_SQL26;
 EXECUTE IMMEDIATE L_SQL27; 
 
 EXECUTE IMMEDIATE L_SQL50;
 EXECUTE IMMEDIATE L_SQL51;
 EXECUTE IMMEDIATE L_SQL52;
 EXECUTE IMMEDIATE L_SQL53;
 EXECUTE IMMEDIATE L_SQL54;
 EXECUTE IMMEDIATE L_SQL55;
 EXECUTE IMMEDIATE L_SQL56;
 EXECUTE IMMEDIATE L_SQL57;
 EXECUTE IMMEDIATE L_SQL58;

 

end;
