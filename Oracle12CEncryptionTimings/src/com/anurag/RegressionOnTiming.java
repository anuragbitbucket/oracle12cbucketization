package com.anurag;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;



import org.apache.commons.io.output.NullOutputStream;





public class RegressionOnTiming {
	
	private String tableName = "CUSTOMER_ALL_ENCRYPTED";

	private static int NUMBER_OF_SIMULATIONS = 100; 
	private static int MAX_NO_KEYS = 50;
	private static PrintStream devNull = new PrintStream(new NullOutputStream());
	private ArrayList<Record> dataGrid = null;
	private ArrayList<Integer> keyCount = new ArrayList<Integer>();
	private ArrayList<Long> timings = new ArrayList<Long>();
	private ArrayList<Long> loadAndDecryptTime = new ArrayList<Long>();
	private long recordCount;
	
	

	
	
	
	public static void main(String[] args) throws SQLException, FileNotFoundException {
		SetupConnection();
		
		RegressionOnTiming rot = new RegressionOnTiming();
		rot.driver();
		rot.printResults();
		
		closeConnection();
		devNull.close();
		
	}
	
	private void printResults() throws FileNotFoundException{
		PrintStream ps = new PrintStream(new FileOutputStream("Results.tsv"));
		
		ps.println("KeyCount\tTime(ns)");
		for(int i=0 ; i<timings.size() ; i++){
			ps.println(keyCount.get(i)+"\t"+timings.get(i));
		}
		ps.println("\n\n\n");
		
		ps.println("Load&Decrypt");
		for(int i =0 ; i<loadAndDecryptTime.size() ; i++){
			ps.println(loadAndDecryptTime.get(i));
		}
		ps.println("\n\n\n");
		ps.println("Number of records:"+this.recordCount);
		ps.close();
		
	}
		
	private void driver() throws SQLException {
		
		cleanBuffers();
		getRecordCount();
		createInMemoryStructures();
		loadTuples();
		
		Random rnd = new Random();
		
		
		
		for(int i=0 ; i<NUMBER_OF_SIMULATIONS ; i++){
			
			if((rnd.nextInt(100) + 1) <= 10){
				//10% probability of reloading
				cleanBuffers();
				createInMemoryStructures();
				loadTuples();
			}
			int numberOfkeys = rnd.nextInt(MAX_NO_KEYS) + 1;
			HashSet<BigDecimal> keysToSearch = new HashSet<BigDecimal>();
			for(int j=1; j<=numberOfkeys ; j++){
				keysToSearch.add(new BigDecimal(rnd.nextInt(100000) + 1));
			}
			search(keysToSearch);
			
		}
					
		
	}
	
	
	
	private void cleanBuffers() throws SQLException {
		PreparedStatement stmt = null;
		try {
			stmt = con.prepareStatement("alter system flush buffer_cache");
			stmt.executeQuery();
			devNull.println("buffer_cache flushed");
		} catch (SQLException e) {
			System.out.println("Error clearing buffer_cache");
			System.out.println(e.getMessage());
		}
		finally{
			stmt.close();
		}
	}
	
	private void search(HashSet<BigDecimal> custKeySet){
		
		long startTime = System.nanoTime();
		for(int i=0 ; i<dataGrid.size() ; i++){
			if(custKeySet.contains(dataGrid.get(i).custKey)){
				devNull.println(i);
			}
		}
		long endTime = System.nanoTime();
		
		keyCount.add(custKeySet.size());
		timings.add(endTime - startTime);
	}
	
	
	private void loadTuples() throws SQLException
    {

		
		String PartialQuery =
        "SELECT C_CUSTKEY , C_NAME , C_ACCTBAL FROM "+ this.tableName;
        

        PreparedStatement ps = con.prepareStatement(PartialQuery);
        
        long startTime = System.nanoTime();;
        ResultSet rs=ps.executeQuery();
        
        Record r = null;
        int i=0;
        while(rs.next()){
        	r=dataGrid.get(i);
        	r.custKey = rs.getBigDecimal(1);
        	r.name = rs.getString(2);
        	r.acctBalance = rs.getBigDecimal(3);
        	++i;
        }
        long endTime = System.nanoTime();
        loadAndDecryptTime.add(endTime- startTime);
        
        
        
    }
	
	private void createInMemoryStructures(){
		dataGrid = new ArrayList<Record>();
		for(long i=0 ; i<recordCount ; i++){
			dataGrid.add(new Record());
		}
		
	}
	
	private void getRecordCount() throws SQLException{
		
		String sql = "SELECT COUNT(*) AS CNT FROM "+this.tableName;
		
		PreparedStatement ps = con.prepareStatement(sql);
		
		ResultSet rs = ps.executeQuery();
		
		long cnt= 0;
		if(rs.next()){
			cnt = rs.getLong(1);
		}
		
		ps.close();
		this.recordCount = cnt;
	}

	
	private static Connection con= null;
	private static String oracleURL = "jdbc:oracle:thin:@localhost:1521/tpch.sensoria.local";
    public static void SetupConnection() throws SQLException{
    	DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
    	con = DriverManager.getConnection(oracleURL, "tpch_user", "abcdef");

    }
    
    public static void closeConnection() throws SQLException{
    	con.close();
    }

}


class Record{
	public BigDecimal custKey;
	public String name;
	public BigDecimal acctBalance;
}

